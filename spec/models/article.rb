class Article
  include Mongoid::Document
  include Mongoid::Timestamps::Short
  #include ActiveModel::ForbiddenAttributesProtection

  field :name

  include Mongoid::Slug
  slug :name

  field :tags

  include Mongoid::Elasticsearch
  i_fields = {
    name: { type: 'text', analyzer: 'snowball' },
    raw: { type: 'text', index: :not_analyzed }
  }

  elasticsearch! index_name: 'mongoid_es_news', prefix_name: false, index_mappings: {
    name: {
      type: 'multi_field',
      fields: i_fields
    },
    _slugs: { type: 'text', index: :not_analyzed },
    tags: { type: 'text', include_in_all: false }
  }, wrapper: :load
end

